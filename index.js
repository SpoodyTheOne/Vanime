'use strict'
const { app, BrowserWindow, ipcMain } = require("electron");
const { Agent } = require("http");
const path = require("path");
const request = require("then-request");

class Scraper {
  static allanime_base = "https://allanime.to";
  static allanime_api = "https://api.allanime.day";
  static agent = "Mozilla/5.0 (Windows NT 6.1; Win64; rv:109.0) Gecko/20100101 Firefox/109.0";
  static headers = { Origin: this.allanime_base, Referer: this.allanime_base, "User-Agent": this.agent };

  static search(name, mode) {
    if (mode != "dub")
      mode = "sub";
    const gql = "query(        \$search: SearchInput        \$limit: Int        \$page: Int        \$translationType: VaildTranslationTypeEnumType        \$countryOrigin: VaildCountryOriginEnumType    ) {    shows(        search: \$search        limit: \$limit        page: \$page        translationType: \$translationType        countryOrigin: \$countryOrigin    ) {        edges {            _id name englishName availableEpisodes __typename thumbnail score episodeCount episodeDuration description        }    }}"
    // const url = `variables={\"search\":{\"allowAdult\":true,\"allowUnknown\":false,\"query\":\"${name}\"},\"limit\":40,\"page\":1,\"translationType\":\"${mode}\",\"countryOrigin\":\"ALL\"}`;
    // const url_encoded = encodeURIComponent(url);

    const variables = {
      "search": {
        "query": name
      },
      "limit": 40,
      "page": 1,
      "translationType": mode,
      "countryOrigin": "ALL"
    };

    const querystring = {
      variables: JSON.stringify(variables),
      query: gql
    };

    console.log("Searching for anime " + name);
    
    const promise = new Promise((resolve, rej) => {
      const final_url = this.allanime_api + "/api";
      request("GET", final_url, { headers: this.headers, qs: querystring }).done((res) => {
        // console.log(res);
        // console.log(res.getBody());
        const data = JSON.parse(res.body.toString("utf-8"));
        // return data.data.shows.edges;
        resolve(data.data.shows.edges);
      });
    });

    return promise;
  }

  static episode(show, episode) {
    const gql = "query (\$showId: String!, \$translationType: VaildTranslationTypeEnumType!, \$episodeString: String!) {    episode(        showId: \$showId        translationType: \$translationType        episodeString: \$episodeString    ) {        episodeString sourceUrls    }}"
  }
}

async function searchAnime(event, name, mode) {
  const data = await Scraper.search(name,mode);

  return data;
}

const room_api_url = "https://b52d9cf2-d697-4cf2-8969-5d73e4f28aae-00-23443avbo892p.spock.replit.dev/";

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js")
    }
  })

  // ipcMain.on("anime-search", (event, name, mode) => {
  //   Scraper.search(name, mode).then(data => {
  //     ipcMain.emit("anime-search-result", data);
  //   });
  // })

  ipcMain.handle("anime-search", searchAnime);
  ipcMain.handle("room-api", async function(event, url) {
    let res = await request("GET", room_api_url + url)

    let ret = {};
    ret.code = res.statusCode;
    ret.body = res.body.toString("utf-8")
    
    return ret;
  })

  win.loadFile('index.html')
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Scraper.search("crimson").then(console.log);
