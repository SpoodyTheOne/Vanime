const { contextBridge, ipcRenderer } = require('electron')

window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
  }

  for (const dependency of ['chrome', 'node', 'electron']) {
    replaceText(`${dependency}-version`, process.versions[dependency])
  }
})


contextBridge.exposeInMainWorld('electronAPI', {
  searchAnime: (name, mode) => ipcRenderer.invoke('anime-search', name, mode),
  roomApi: (url) => ipcRenderer.invoke("room-api", url)
})

