// setTimeout(() => {
//   const butn = document.getElementById("sub");
//   const src = document.getElementById("search");
//   /** @type{HTMLTextAreaElement} */
//   const area = document.getElementById("output")

//   butn.onclick = async () => {
//     const data = await window.electronAPI.searchAnime(src.value);
//     // area.value = JSON.stringify(data.map(x => { return { name: x.name, episodes: x.availableEpisodes?.sub } }));
//     area.value = JSON.stringify(data);
//   }
// }, 100);

async function createRoom(name) { };

async function init() {
  const grid = document.querySelector("#anime-grid");
  const animeTemplate = document.querySelector("#anime-grid .anime");
  const roombtn = document.querySelector("#burger #room");
  const roomui = document.querySelector("#room-manager");

  let roomid = null;
  let userid = null;

  /// Creates a new room and returns the roomcode
  async function createRoom(name) {
    startThrob()
    if (name == null)
      throw Error("fuck you need a name!!");

    try {
      roomid = (await roomApi("/room/create")).body;
      if (name != null)
        userid = (await roomApi(`/room/${roomid}/join/${name}`)).body;
    } catch (e) {
      alert("Server isnt up yet :P");
      stopThrob();
      return false;
    }

    stopThrob();
    return true;
  }

  async function joinRoom(room, mane) {
    startThrob()
    try {
      userid = (await roomApi("/room/" + room + "/join/" + mane)).body;
      roomid = room;
    } catch (e) {
      if (e.code == 404) {
        alert("Room does not exist");
      }
      stopThrob();
      return false;
    }

    stopThrob()
    return true;
  }

  roombtn.onclick = () => {
    roomui.classList.remove("hidden");
  }

  document.querySelector("#room-manager .roomform").onsubmit = async (ev) => {
    ev.preventDefault();

    const rid = document.querySelector(".roomidinput").value;
    const rnam = document.querySelector(".roomname").value;

    let status = false;

    if (rid == "")
      status = await createRoom(rnam);
    else
      status = await joinRoom(rid, rnam);

    if (status) {
      document.querySelector(".roomid").textContent = roomid;
      document.querySelector(".username").textContent = rnam;
      roombtn.textContent = "Room: " + roomid;
    }
  }

  window.createRoom = createRoom;

  function addAnime(title, rating, runtime, thumbnail, description) {
    /** @type{HTMLDivElement} */
    const clone = animeTemplate.cloneNode(true);

    clone.className = "anime";

    clone.querySelector(".name").textContent = title;
    // clone.querySelector(".thumbnail").src = thumbnail;
    clone.querySelector(".thumbnail").style.backgroundImage = `url(${thumbnail})`

    let ratingStars = `<i class="fa-solid fa-star"></i>${rating ? rating : "0"}`;
    const ratingElement = clone.querySelector(".rating");
    ratingElement.innerHTML = ratingStars;

    let runtimehtml = `<i class="fa-solid fa-clock"></i>${msToTime(runtime)}`;
    clone.querySelector(".runtime").innerHTML = runtimehtml;

    clone.querySelector(".description").innerHTML = parseDescription(description);

    grid.appendChild(clone);

    clone.onclick = (ev) => {
      if (clone.classList.contains("viewing") && ev.target != clone) {
        ev.preventDefault();
        return;
      }
      clone.classList.toggle("viewing");
    }
  }

  /** @param {string} d */
  function parseDescription(d) {
    d = d?.replace(/(<script>|onclick|onload|onkeydown)/g, "UNSAFE VALUE");
    return d;
  }

  function msToTime(ms) {
    const seconds = Math.floor((ms / 1000) % 60);
    const minutes = Math.floor((ms / 1000 / 60) % 60);
    const hours = Math.floor((ms / 1000 / 60 / 60) % 24);

    if (hours > 0) {
      return `${hours}h ${minutes.toString().padStart(2, "0")}m`
    }

    return `${minutes}m`
  }

  /** @type{HTMLFormElement} */
  const searchbox = document.querySelector("#searchbox");
  const searchInput = searchbox.querySelector("input.searchtext")
  const searchSubmit = searchbox.querySelector("input.submit")

  searchbox.onsubmit = (ev) => {
    ev.preventDefault();

    displayAnime(searchInput.value);
    searchbox.classList.add("hidden");

    const animes = document.querySelectorAll("#anime-grid .anime:not(.disabled)");
    animes.forEach(a => {
      a.remove();
    })
  }

  searchbox.onclick = (ev) => {
    if (ev.target != searchbox)
      return;

    searchbox.classList.add("hidden")
  };

  searchAnime = async function(name, mode) {
    throbber.classList.remove("hidden");

    const ret = await window.electronAPI.searchAnime(name, mode);

    throbber.classList.add("hidden");
    return ret;
  }

  roomApi = async function(url) {
    let result = await window.electronAPI.roomApi(url);
    if (result.code >= 300) {
      let err = new Error(result.code + ": " + result.body);
      err.code = result.code;
      err.body = result.body;
      throw err;
    }

    return result;
  }

  async function displayAnime(name, mode) {
    const data = await searchAnime(name, mode);
    console.log(data);

    data.forEach(v => {
      addAnime(v.englishName || v.name, v.score, v.episodeDuration, v.thumbnail, v.description);
    })
  }

  document.getElementById("search").onclick = () => {
    searchbox.classList.remove("hidden");
    searchInput.value = "";
    searchInput.focus();
  }
  const throbber = document.getElementById("throbber_container")

  function startThrob() {
    throbber.classList.remove("hidden");
  }

  function stopThrob() {
    throbber.classList.add("hidden");

  }

}
setTimeout(init, 100);

